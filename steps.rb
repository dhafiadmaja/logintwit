require 'selenium-webdriver'

#options = Selenium::WebDriver::Chrome::Options.new
#options.add_argument('--disable-notifications')
#options.add_argument('--no-sendbox')
#driver = Selenium::WebDriver.for :chrome, options:options
driver = Selenium::WebDriver.for :firefox
Given("I Open the twitter homepage") do
  driver.navigate.to "https://twitter.com/"
end
  Then("I Login using id and password") do
    driver.find_element(:xpath, '/html/body/div[1]/div/div[1]/div[1]/div[2]/div/a').click
    driver.find_element(:xpath, '/html/body/div[1]/div[2]/div/div/div[1]/form/fieldset/div[1]/input').send_keys("root@login")
    driver.find_element(:xpath, '/html/body/div[1]/div[2]/div/div/div[1]/form/fieldset/div[2]/input').send_keys("admin")
    driver.find_element(:xpath, '/html/body/div[1]/div[2]/div/div/div[1]/form/div[2]/button').click
end
